<?php

class IndexController extends ControllerBase
{

    public function indexAction()
    {
        //$this->session->set("user-name", "Michael");
        $user_info= $this->session->get("user_info");
        if(!$user_info){
            $this->response->redirect('login/index');
        }
    }

}

